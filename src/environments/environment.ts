// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyBR5VHlsEQ7uQLpyhpWWVQyBDqDVa20tnE",
    authDomain: "sales-admin-4abde.firebaseapp.com",
    projectId: "sales-admin-4abde",
    storageBucket: "sales-admin-4abde.appspot.com",
    messagingSenderId: "41262996627",
    appId: "1:41262996627:web:6a4805841d1b8f60720166",
    measurementId: "G-JN7VLGW0DQ"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
