import { novoCliente } from 'src/app/models/clientes.interface';
import { ClienteInterface } from './../../../models/clientes.interface';
import { Component, OnInit } from '@angular/core';

import {
  DialogService,
  DynamicDialogConfig,
  DynamicDialogRef,
} from 'primeng/dynamicdialog';
import {
  collapseOnLeaveAnimation,
  fadeInExpandOnEnterAnimation,
} from 'angular-animations';
import * as moment from 'moment';
import { ClienteService } from 'src/app/services/clientes.service';
import * as uuid from 'uuid-random';
@Component({
  templateUrl: './cliente-modal.component.html',
  styleUrls: ['./cliente-modal.component.scss'],
  animations: [fadeInExpandOnEnterAnimation(), collapseOnLeaveAnimation()],
})
export class ClienteModalComponent implements OnInit {
  modalTitle = '';
  isEdiableSwitchArr = [
    { label: 'Info', value: 'info' },
    { label: 'Edit', value: 'edit' },
  ];
  modalState = '' as 'info' | 'edit' | 'add';
  clientItem: ClienteInterface = novoCliente();
  constructor(
    public dialogRef: DynamicDialogRef,
    private clientSrv: ClienteService,
    private config: DynamicDialogConfig
  ) {}

  ngOnInit(): void {
    console.log(this.config.data);
    this.clientItem = this.config.data.clientItem;
    this.modalState = this.config.data.modalState;

    this.modalTitle = 'Novo cliente';
  }
  onCepChange(ev: string) {
    this.clientSrv.getEndereco(ev).subscribe((res) => {
      console.log(res);
      this.clientItem.endereco.cep = res.cep;
      this.clientItem.endereco.bairro = res.bairro;
      this.clientItem.endereco.complemento = res.complemento;
      this.clientItem.endereco.localidade = res.localidade;
      this.clientItem.endereco.logradouro = res.logradouro;
      this.clientItem.endereco.uf = res.uf;
    });
  }
  get isEditable() {
    let ret = true;
    if (this.modalState === 'info') {
      ret = false;
    }
    return ret;
  }

  onModalStateChange(ev:any){

  }
  saveItem() {
    if (this.clientItem.id !== '') {
      this.clientItem.updatedAt = new Date().getTime();
    } else {
      this.clientItem.createdAt = new Date().getTime();
      this.clientItem.updatedAt = new Date().getTime();
      this.clientItem.id = uuid();
    }
    this.clientSrv.saveClient(this.clientItem).then((res) => {
      this.dialogRef.close('');
    });
  }
}
