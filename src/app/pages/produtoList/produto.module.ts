import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ProdutoPageRoutingModule } from './produto-routing.module';
import { ProdutoModalComponent } from './produto-modal/produto-modal.component';
import { ProdutoPage } from './produto.page';

import { AgGridModule } from 'ag-grid-angular';
import { ComponentsModule } from '../../components/component.module';
import { PrimeNGModule } from './../../modules/primeNg.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PrimeNGModule,
    AgGridModule,
    ComponentsModule,
    ProdutoPageRoutingModule,
  ],
  declarations: [
    ProdutoPage,
    ProdutoModalComponent,
  ],
})
export class ProdutoPageModule {}
