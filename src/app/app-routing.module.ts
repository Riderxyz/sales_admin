import { HomeComponent } from './pages/home/home.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'home',
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'clienteList',
    loadChildren: () => import ('./pages/clienteList/cliente.module').then((m) => m.ClientePageModule)
  },
  {
    path: 'produtosList',
    loadChildren: () => import ('./pages/produtoList/produto.module').then((m) => m.ProdutoPageModule)
  },
  {
    path: 'vendaList',
    loadChildren: () => import ('./pages/vendaList/venda.module').then((m) => m.VendaPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
