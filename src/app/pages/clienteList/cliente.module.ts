import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ClientePageRoutingModule } from './cliente-routing.module';
import { ClienteModalComponent } from './cliente-modal/cliente-modal.component';
import { ClientePage } from './cliente.page';

import { AgGridModule } from 'ag-grid-angular';
import { PrimeNGModule } from './../../modules/primeNg.module';
import { ComponentsModule } from '../../components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PrimeNGModule,
    AgGridModule,
    ComponentsModule,
    ClientePageRoutingModule,
  ],
  declarations: [
    ClientePage,
    ClienteModalComponent,
  ],
})
export class ClientePageModule {}
