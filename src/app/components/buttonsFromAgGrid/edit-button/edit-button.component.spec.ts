import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgGridEditButtonComponent } from './edit-button.component';

describe('AgGridEditButtonComponent', () => {
  let component: AgGridEditButtonComponent;
  let fixture: ComponentFixture<AgGridEditButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgGridEditButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgGridEditButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
