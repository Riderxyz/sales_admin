import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  items: MenuItem[] = [];
  constructor() {}
  ngOnInit(): void {
    this.items = [
      {
        label: 'Lista Produtos',
        // icon:'fas fa-basketball-ball',
        routerLink: ['/produtosList'],
      },
      {
        label: 'Lista Clientes',
        //icon:'fas fa-basketball-ball',
        routerLink: ['/clienteList'],
      },
      {
        label: 'Lista de vendas feitas',
        //icon:'fas fa-basketball-ball',
        routerLink: ['/vendaList'],
      },

      /*  {
          label: 'Edit',
          icon: 'pi pi-fw pi-pencil',
          items: [
              {label: 'Delete', icon: 'pi pi-fw pi-trash'},
              {label: 'Refresh', icon: 'pi pi-fw pi-refresh'}
          ]

          <i class="fas fa-basketball-ball"></i>
          <i class="fas fa-football-ball"></i>
          <i class="fas fa-futbol"></i>
          <i class="fas fa-volleyball-ball"></i>
      } */
    ];
  }
}
