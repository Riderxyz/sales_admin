import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgGridDeleteButtonComponent } from './delete-button.component';

describe('AgGridDeleteButtonComponent', () => {
  let component: AgGridDeleteButtonComponent;
  let fixture: ComponentFixture<AgGridDeleteButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgGridDeleteButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgGridDeleteButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
