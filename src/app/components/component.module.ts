import { CdkModule } from './../modules/cdk.module';
import { PrimeNGModule } from './../modules/primeNg.module';
import { NgModule } from '@angular/core';
import { HeaderComponent } from './header/header.component';

import { AgGridEditButtonComponent } from './buttonsFromAgGrid/edit-button/edit-button.component';
import { AgGridDeleteButtonComponent } from './buttonsFromAgGrid/delete-button/delete-button.component';


@NgModule({
  imports: [
    PrimeNGModule,
    CdkModule
  ],
  exports: [HeaderComponent],
  declarations: [
    HeaderComponent,
    AgGridEditButtonComponent,
    AgGridDeleteButtonComponent
  ],
  providers: [],
})
export class ComponentsModule { }

