import { config } from './config';
import { ClienteInterface } from './../models/clientes.interface';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EnderecoInterface } from '../models/endereco.interface';
import { tap, take, debounceTime } from 'rxjs/operators';
import { AngularFirestore } from "@angular/fire/firestore";

@Injectable({providedIn: 'root'})
export class ClienteService {
  private _viaCepUrl = 'https://viacep.com.br/ws';
  constructor(
    private http: HttpClient,
    private db: AngularFirestore) { }

  getEndereco(cep: string) {
    const url = `${this._viaCepUrl}/${cep}/json`;
    return this.http
      .get<EnderecoInterface>(url)
      .pipe(debounceTime(1500));
  }

  get clientList() {
    return this.db
    .collection<ClienteInterface>(config.url.dbClient)
    .valueChanges();
  }
  saveClient(ev: ClienteInterface) {
    return this.db.collection(config.url.dbClient).doc(ev.id).set(ev);
  }
}
/* tap((_) => console.log(`fetched endereco cep=${cep}`)), */
