import { ClienteService } from 'src/app/services/clientes.service';
import { ClienteInterface } from './../../models/clientes.interface';
import { ClienteModalComponent } from './cliente-modal/cliente-modal.component';

import { AgGridEditButtonComponent } from '../../components/buttonsFromAgGrid/edit-button/edit-button.component';
import { AgGridDeleteButtonComponent } from '../../components/buttonsFromAgGrid/delete-button/delete-button.component';
import { Component, OnInit } from '@angular/core';
import {
  GridOptions,
  GridApi,
  ColumnApi,
  ColDef,
  RowDoubleClickedEvent,
  SelectionChangedEvent,
  RowClickedEvent,
} from 'ag-grid-community';
import { delay } from 'rxjs/operators';
import * as moment from 'moment';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ConfirmationService, ConfirmEventType } from 'primeng/api';

import {
  flipOnEnterAnimation,
  fadeInOnEnterAnimation,
  fadeOutOnLeaveAnimation,
} from 'angular-animations';

import * as lodash from 'lodash';
import { novoCliente } from 'src/app/models/clientes.interface';
@Component({
  templateUrl: './cliente.page.html',
  styleUrls: ['./cliente.page.scss'],
  providers: [DialogService, ConfirmationService],
  animations: [
    flipOnEnterAnimation(),
    fadeInOnEnterAnimation(),
    fadeOutOnLeaveAnimation(),
  ],
})
export class ClientePage implements OnInit {
  clienteArr: ClienteInterface[] = [];
  gridOptions!: GridOptions;
  gridApi!: GridApi;
  filter = '';
  modalInfoRef!: DynamicDialogRef;
  modalInfoShow = false;
  contestColumnDef = [
    {
      headerName: 'ID',
      field: 'id',
      /* sort: 'asc', */
      hide: true,
      width: 40,
      autoHeight: true,
    },
    {
      headerName: 'Nome',
      field: 'nome',
      width: 200,
    },
    {
      headerName: 'Ramo ou Atividade',
      field: 'ramoOuAtividade',
      width: 200,
    },
    {
      headerName: 'Edit',
      cellRenderer: 'editButtonRenderer',
      cellRendererParams: {
        onClick: this.onEditButtonClick.bind(this),
        label: 'Delete',
      },
      width: 80,
    },
    {
      headerName: 'Delete',
      cellRenderer: 'deleteButtonRenderer',
      cellRendererParams: {
        onClick: this.onDeleteButtonClick.bind(this),
        label: 'Delete',
      },
      width: 80,
    },
  ] as ColDef[];
  colunasFiltraveis = ['name', 'creationDateTime', 'status'];
  isGameOnLoading = false;
  constructor(
    public dialogService: DialogService,
    private confirmationService: ConfirmationService,
    private clientSrv: ClienteService
  ) {
    this.startGrid();
  }

  ngOnInit(): void {
    this.clientSrv.clientList.subscribe((res) => {
      console.log(res);
      this.clienteArr = res;
      this.newGamesData();
    })
  }

  startGrid() {
    this.gridOptions = {
      rowData: this.clienteArr,
      columnDefs: this.contestColumnDef,
      rowSelection: 'single',
      defaultColDef: {
        editable: false,
        sortable: true,
        autoHeight: true,
        resizable: true,
        filter: true,
      },
      frameworkComponents: {
        deleteButtonRenderer: AgGridDeleteButtonComponent,
        editButtonRenderer: AgGridEditButtonComponent,
      },
      isExternalFilterPresent: this.externalFilterPresent.bind(this),
      doesExternalFilterPass: this.externalFilterPass.bind(this),
      animateRows: true,
      /*  overlayNoRowsTemplate: `
        <span class='ag-overlay-loading-center'>Sem dados para exibir</span>
        `, */
      onRowDoubleClicked: (params) => {
        this.showModal(params.data as any, 'info');
      },
      overlayLoadingTemplate: `<span'>Loading data</span>`,
      /*       onRowClicked: (params) => {
        // this.onRowSelected(params);
      }, */

      onGridReady: (event: { api: any }) => {
        console.log('carreguei a grid', this.clienteArr);
        /* debugger; */
        this.gridApi = event.api;
        this.gridApi.sizeColumnsToFit();
        this.newGamesData();
      },
    };
  }

  newGamesData() {
    this.isGameOnLoading = !this.isGameOnLoading;
    this.gridApi.setRowData(this.clienteArr);
    this.gridApi.sizeColumnsToFit();
    this.isGameOnLoading = !this.isGameOnLoading;
  }
  externalFilterPresent() {
    return this.filter !== '';
  }
  externalFilterPass(node: any) {
    let retorno = false;
    this.colunasFiltraveis.forEach((element) => {
      if (this.filter !== '') {
        if (
          (node.data[element] + '')
            .toLowerCase()
            .indexOf(this.filter.toLowerCase()) !== -1
        ) {
          retorno = true;
        }
      } else {
        retorno = false;
      }
    });
    return retorno;
  }

  onFilterChange() {
    if (this.gridApi !== undefined) {
      this.gridApi.onFilterChanged();
    }
  }
  onAddButtonClick() {
    // this.modalInfoShow = true;
    this.showModal(novoCliente(), 'add');
  }
  onEditButtonClick(params: any) {
    this.showModal(params, 'edit');
  }
  onDeleteButtonClick(params: any) {
    console.log('retornando ', params);
    // this.dataSrv.excluirBanco(params.codigo);
    this.confirmationService.confirm({
      message: 'Are you sure that you want to proceed?',
      acceptButtonStyleClass: 'p-button-success',
      dismissableMask: true,
      rejectButtonStyleClass: 'p-button-danger',
      header: 'Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        console.log('linha 209');
        let index = lodash.findIndex(this.clienteArr, (el: any) => {
          return el.id == params.id;
        });
        this.clienteArr.splice(index, 1);
        this.gridApi.setRowData(this.clienteArr);
      },
      reject: () => { },
    });
  }
  async showModal(clientItem: ClienteInterface,  modalState: 'info' | 'edit' | 'add') {
console.log(clientItem);
    this.modalInfoRef = this.dialogService.open(ClienteModalComponent, {
      data: {
        clientItem,
        modalState,
      },
      dismissableMask: true,
      closeOnEscape: true,
      showHeader: false,
      width: '70%',
      contentStyle: { padding: '0em' },
      baseZIndex: 10000,
    });
    //'max-height': '600px',
    this.modalInfoRef.onClose.subscribe((data) => {
      console.log(data);
     /*  if (data) {
        // let contestItem = new Conten
        //gameItem = data;
        let index = lodash.findIndex(this.clienteArr, (el: any) => {
          return el.id == data.id;
        });
        this.clienteArr[index] = data;
        this.gridApi.setRowData(this.clienteArr);
      } */
    });
  }
}
