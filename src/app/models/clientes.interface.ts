import { EnderecoInterface } from './endereco.interface';
export interface ClienteInterface {
  id: string;
  nome: string;
  empresa?: string;
  endereco: EnderecoInterface;
  ramoOuAtividade: string;
  createdAt: number;
  updatedAt: number;
  contato: {
    email: string;
    telefone: {
      fixo: string;
      celular: string;
    };
    whatsApp?: string;
  };
}

export const novoCliente = (): ClienteInterface => {
  return {
    id: '',
    nome: '',
    empresa: '',
    endereco: {
      uf: '',
      cep: '',
      bairro: '',
      logradouro: '',
      complemento: '',
      localidade: '',
      numero: 0,
    },
    ramoOuAtividade: '',
    createdAt: 0,
    updatedAt: 0,
    contato: {
      email: '',
      telefone: {
        fixo: '',
        celular: '',
      },
      whatsApp: '',
    },
  };
};

export const validateCliente = ((client: ClienteInterface): string[]=> {
let retorno = [];

  return [];
})
