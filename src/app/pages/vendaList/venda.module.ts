import { VendaPageRoutingModule } from './venda-routing.module';
import { VendaPage } from './venda.page';
import { VendaModalComponent } from './venda-modal/venda-modal.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { PrimeNGModule } from './../../modules/primeNg.module';
import { AgGridModule } from 'ag-grid-angular';
import { ComponentsModule } from '../../components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PrimeNGModule,
    AgGridModule,
    ComponentsModule,
    VendaPageRoutingModule,
  ],
  declarations: [
    VendaPage,
    VendaModalComponent,
  ],
})
export class VendaPageModule {}
