import { Component, OnInit } from '@angular/core';

import {
  DialogService,
  DynamicDialogConfig,
  DynamicDialogRef,
} from 'primeng/dynamicdialog';
import {
  collapseOnLeaveAnimation,
  fadeInExpandOnEnterAnimation,
} from 'angular-animations';
import * as moment from 'moment';
@Component({
  templateUrl: './produto-modal.component.html',
  styleUrls: ['./produto-modal.component.scss'],
  animations: [fadeInExpandOnEnterAnimation(), collapseOnLeaveAnimation()],
})
export class ProdutoModalComponent implements OnInit {
  isEdiable = false;
  sportArr = [];
  modalTitle = '';
  isEdiableSwitchArr = [
    { label: 'Info', value: false },
    { label: 'Edit', value: true },
  ];
  customDt = {
    customStartDateTime: '',
    customEndDateTime: '',

  }
  constructor(
    public dialogRef: DynamicDialogRef,
    private config: DynamicDialogConfig,
    ) {
    console.log(this.config.data);

    this.isEdiable = this.config.data.isEditable;

    this.modalTitle = 'this.gameItem.name;'
  }

  ngOnInit(): void {
/*     this.customDt.customStartDateTime = moment(
      this.gameItem.startDateTime
    ).format('L hh:mm');
    this.customDt.customEndDateTime = moment(
      this.gameItem.endDateTime
    ).format('L hh:mm');

    this.gameSrv.listAllSports.subscribe((resSports) => {
      console.log(resSports);
    })
    this.gameSrv.listAllLeagues.subscribe((resLeague) => {
      console.log(resLeague);
    }) */
  }


  onCustomDtChange() {
   /*  if(this.customDt.customStartDateTime){
     this.gameItem.startDateTime = new Date(this.customDt.customStartDateTime.replace(/[a-z]/gi, ''));
    }
    if(this.customDt.customEndDateTime){
      this.gameItem.endDateTime = new Date(this.customDt.customEndDateTime.replace(/[a-z]/gi, ''));
    }

    console.log(this.customDt);
 */

  }



  saveItem() {
    /*     if (this.contestItem.id !== '') {
      this.payoutArr.forEach((el, i) => {
        if (el.value !== 0) {
          if (this.contestItem.payouts[i]) {
            this.contestItem.payouts[i].payout = el.value;
          } else {
            this.contestItem.payouts.push({
              payout: el.value,
              places: [],
            });
          }
        }
      });
    } */
    /*    this.contestSrv.saveContest(this.contestItem.payload).subscribe((ev) => {
    }); */
    this.dialogRef.close('this.gameItem');
  }
}
